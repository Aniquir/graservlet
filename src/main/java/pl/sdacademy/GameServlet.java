package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
/*
komentarz do poma:           //dodajemy to i mozemy korzystac z tamtego utils do tego projektu, ale te zmozemy to wykorzystac od innych, zeby
            //generowac htmla
 */
@WebServlet(value = "/game")
public class GameServlet extends HttpServlet {
//robimy projekt, ktory jest gra

    private Random generator = new Random();
    private int randomNumber;
    private int tryCounter;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = getPrintWriter(resp);
        HtmlGenerator.generateHeader(out, "gra w zgadywanie");
        out.println("Witaj na stronie z gra<br>");
        out.println("wylosowalem liczbe od 1 do 10, spróbuj ją zgadnąc<br>");
        generateForm(out);

        HtmlGenerator.generateFooter(out);
        randomNumber = generator.nextInt(10)+1;
        tryCounter = 0;
    }

    private int getParameter(HttpServletRequest request, String parameterName) {
        try {
            return Integer.parseInt(request.getParameter(parameterName));
        } catch (NumberFormatException exception) {
            return -1;
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //dodac zmienna tryCounter
        tryCounter++;
        PrintWriter out = getPrintWriter(resp);
        HtmlGenerator.generateHeader(out, "gra w zgadywanie");
        out.println("liczba prób: " + tryCounter + "<br>");
        int usersNumber = getParameter(req,"podanaLiczba");

        if (usersNumber < randomNumber){

            out.println("podana liczba jest za mala<br>");
            generateForm(out);
        }else if (usersNumber > randomNumber){
            out.println("podana liczba jest za duza<br>");
            generateForm(out);
        } else {
            out.println("<h1>brawo! </h1><br> --- udalo Ci sie odgadnac --- <br>");
            out.println("<a href=\"/szablon/game\">Zagraj jeszcze raz</a>");
        }

        HtmlGenerator.generateFooter(out);
    }
    private PrintWriter getPrintWriter(HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset-utf-8");
        response.setCharacterEncoding("utf-8");
        return response.getWriter();
    }

    private void generateForm(PrintWriter out) {
        out.println("<form action=\"/szablon/game\" method=\"post\">");
        out.println("podaj liczbe: ");
        out.println("<input type=\"text\" name=\"podanaLiczba\">");
        out.println("<input type=\"submit\" value=\"Przeslij\">");
        out.println("</form");
    }
//poprawic, zeby nie wyswietlalo sie podwojnie na stronie
}
